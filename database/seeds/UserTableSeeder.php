<?php

use Illuminate\Database\Seeder;
use App\User;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Create Admin User
        $user= new User;
        $user->name = 'Admin User';
        $user->email = 'admin@123.com';
        $user->password = bcrypt('12345678');
        $user->user_role = 'admin';
        $user->save();

        //Create Dealer User
        $user= new User;
        $user->name = 'Dealer User';
        $user->email = 'dealer@123.com';
        $user->password = bcrypt('12345678');
        $user->user_role = 'dealer';
        $user->save();
    }
}
