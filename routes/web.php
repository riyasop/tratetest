<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::any('/register',function(){
        return redirect('/login');
});
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/update-profile', 'HomeController@updateprofile')->name('updateprofile');
Route::get('/profile-image', 'HomeController@profileimage')->name('profileimage');
