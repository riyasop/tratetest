<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = auth()->user();
        return view('profile',compact('user'));
    }
    public function updateprofile(Request $request)
    {
        $validate_fields=[
            'name' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ];
        $validate_messages=[
        ];
        $this->validate($request, $validate_fields,$validate_messages);
        
        $error='';
        DB::beginTransaction();
        try {
            $filename="";
            if($request->file('image'))
            {
                $filename = 'ProfileImage_'.time().'.'.$request->file('image')->getClientOriginalExtension();
                $request->file('image')->storeAs('profile_images', $filename);
                if(auth()->user()->image)
					 \File::delete(storage_path('app/profile_images/'.auth()->user()->image));
            }
            
            $user = User::find(auth()->user()->id);
            $user->name = $request->name;
            if($filename!="")
                $user->image = $filename;
            $user->save();
            
            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $error=$e->getMessage();
            $success = false;
            DB::rollback();
        }

        if ($success) {
            
            return back()
            ->with('status','Profile update successful');
        }
        else
        {
            return back()
            ->with('status', 'There were some problems with your input');
        }

    }
	public function profileimage()
    {
        return response()->download(storage_path('app/profile_images/' . auth()->user()->image));
    }
}
